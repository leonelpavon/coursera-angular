"Lista de deseos" es un proyecto que se inició con motivo del curso "Desarrollo de páginas con Angular" a través de la web de aprendizaje Coursera. A través de la utilización de los componentes de Angular, construimos nuestra SPA -sigla en inglés de 'Single Page Application'.
La misma contiene un home en donde se pueden agregar nombres de destinos y una URL, que se guarda luego en la parte inferior y que contiene información detallada e imágenes a modo de ejemplo. 

Para más info pueden visitar: https://www.coursera.org/learn/desarrollar-paginas-web-con-angular