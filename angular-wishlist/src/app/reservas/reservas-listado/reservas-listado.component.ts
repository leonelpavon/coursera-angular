import { Component, OnInit } from '@angular/core';
import { DestinosApiClient } from 'src/app/models/destinos-api-client.model';
import { ReservasApiClientService } from '../reservas-api-client.service';

@Component({
  selector: 'app-reservas-listado',
  templateUrl: './reservas-listado.component.html',
  styleUrls: ['./reservas-listado.component.css'], 
  providers: [ DestinosApiClient ]
})
export class ReservasListadoComponent implements OnInit {

  constructor (public api: ReservasApiClientService) { }

  ngOnInit(): void {
  }

}
